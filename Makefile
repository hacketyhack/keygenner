C_keygen: C_keygen.c
	gcc -Wall -Wextra -Wpedantic -Wshadow -g -o C_keygen C_keygen.c

clean: 
	rm -f C_keygen

.PHONY: clean
