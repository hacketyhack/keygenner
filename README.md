# lvl1 Keygen Crackme - OS Linux-Ubuntu

## Setup:
- Download the file from [crackmes.one](https://crackmes.one/)
- We can use `file keygen.bin` to check the type of the file. In this 
case it will be an `ELF`. 

## Assembly:
See [assembly_keygen.s](assembly_keygen.s)
In the assembly step we specifically looked for comparisons and jump statements 
that would indicate an equivalence to an if statement.
This allows us to more easily map the programs flow and branches. Using gdb we can 
set break points, check register values, and dump assembly instructions in small
increments to help us better analyze what is occuring. 

- On the command line enter `objdump -M intel -D keygen.bin | gvim -`. 
This pipes the disassbly into vim so that we can save the assembly
into a `.s` file. From here we can make notes about the program.
- At a cursory glance, there were the expected labels `<plt.got>`(procedure linkage table, global offset table),
`<.plt>` (procedure linkage table), `<_init>` etc. 
These are standard setup to any program that is setup by the linker.
- We are specifically looking for label names of interest, such as; `<main>`, and `<check_keys>`.
These would be a reasonable guess to where interesting information about this keygen can be found.

### `main`:
- [Lines 226-232](assembly_keygen.s#L226-232) setup the stack.
- Particularly interesting, we see room is made of 0x68 on the stack for a variable to be stored ([line 228](assembly_keygen.s#L228)) + 0x8 used for the stack cookie. But nothing of interest as of yet seems to have been put in this memory allocation.
- Next the program prepares the stack once again, this time for a function call to `puts()`. This we can deduce is presenting the first line of output to the screen: `Give me a pass`.
- Followed by a call to `scanf()`. Here we see the the program takes the input from the user
and saves it into the allocated memory `rbp-0x70` we saw allocated at the start of the program.
- The program then makes a call to our other function of interest `check_keys`.
- Upon returning from `check_keys`, the program tests the register `eax` to check if
the value returned from the function is the numeric value for `true` or `false`.
- If true, the program prints the success message: `You made it, now keygen me!`. (The false case messages are printed within the `check_keys` function.) 
- In either case the program then proceeds to clean up the stack and exits.

### `check_keys`:
See [check_keys](assembly_keygen.s#L145)
- The function prepares the stack.
- It then proceeds to intialize the local variables. We can see that room is made for 3 variables ([line 155-157](assembly_keygen.s#L155-157)).
- We can deduce from [lines 160-162](assembly_keygen.s#L160-162), that our user input has been saved in `rbp-0x28`. Upon which
the function string length is being called, the result of which will be in `rax`.

### Branching: 
- This then leads to our first if statement and branch in the program: 
The returned string length value is then compared to 0x7. If the string length is below or equal to 0x7 the program
prints the failure message and calls exit, thus terminating the entire program.
- Otherwise, the program falls through (continues), and calls string length once again on our
user's input. This time the condition is checked to see if string length is greater than 0xa (10).
If below or equal to 10, the program jumps to what appears to be a loop setup. Otherwise,
the program falls through, resets `eax`, makes a call to `print()` to produce the failure message, then
proceeds to exit. Thus terminating the program.

### The Loop:
To best understand the loop, we used gdb to set break points, running assembly dumps on small sections at a time using `disas`, and `info r` to check the values stored in the registers. This allows us to see what values where being added to the various variables we notice addition being done on. Further, this helped to identify a further conditional statement that
was being checked in the determination of a correct password.

The overarching purpose of the loop is to iterate across the entire user given string. As it goes it sums up the ascii value for each character, at the end of which it checks if the strings entire value is greater than 999. This is how the program determines if a given password is acceptable.

#### Loop Setup:
- In the loop setup we see that `rbp-0x18` (the loop counter) gets set to 0x0. Then the program proceeds to the
loop condition.
#### Loop Condition:
- The loop condition checks that the value in `rbp-0x18`, which has now been stored in `rbx`, is
equal to the length of the user's input string (by making a string length call on user input stored in `rbx-0x28`).
- If this value in `rbp-0x18`, lets call it our counter, is below the string length the program jumps to the top of the loop. 
- Otherwise, the program then runs a comparison on a value stored in `rbp-0x14` to the value `0x3e7` (999). For simplicity lets call this variable in `rbp-0x14` sum. If our variable sum is greater then 999, the function returns true. Thus returning control back to the `<main>`. 
- Otherwise, the program prints the failure message, and exits the entire program.
#### Inside the Loop:
- The counter appears to be used to track our loops progress iterating over the user's input string. This can be seen on [lines 190-197](assembly_keygen.s#L190-197).
- The character that our counter is on in the string is taken and its ascii numeric value is added to our sum variable.
- The counter is then incremented by 1, before going to the loop condition once more.

## KeyGen Script:
See [keygen.py](keygen.py)

We chose to write this script in Python3.7. The required imports were `subprocess` for calling another program and piping our program's input into the subprocess. Another required package is `string` to acquire string functions. Along with `random` to help us generate a new random password everytime.

```console
shazari@wonderland:~/code/crackmes/keygenner$ ./keygen.py
Generated password: _xVw[rznr_
Give me a pass
You made it, now keygen me!
```

### pw_generator():
- The integer `ascii_val` variable will hold the sum of our string's ascii value.
- `password` starts as an empty string, but will be built up via each pass of our for loop.
- While Loop: The program loops on the condition that our ascii_val is less than or equal to 999.
Otherwise, the program will break out of the loop returning the string stored in `password`
from the function to the caller.
- If Statement: First, we check if the password length is greater than or equal to 10, if so, we reset everything.
the counter `i` and `ascii_val` go back to 0, the `password` string we were building up is reset to the
empty string. In short, we have reached the max length allowed for the password, but our randomly generated password's 
ascii value did not exceed 999, thus an unacceptable password.
-For Loop: The loop increments to the max length allowed for a password.
- `c` is a string variable that holds a single ascii character selected at random from our list of allowed ascii characters.
- `ascii_val` is incremented by the ascii value of `c` by a call to `ord()`.
- `password` is then appended to with the string value in c.

### main():
- Creates a string variable `password` we will assign the value produced from
`pw_generator()` to it.
- The program then calls `subprocess.run()` using the command used on the command
line to execute a program `./` with the file name `keygen.bin` as the program we want
execute, followed by our `password` varialbe for the argument to `input`. We want this 
to be read as text, thus `text=True`.
- After calling the `pw_generator()`, we print the newly generated password.
- We included a `print()` to add a line break once the program returns to the command line.
This is a personal stylistic choice

## Reconstructing
See [C_keygen.c](C_keygen.c)
As an added exercise we decided to rebuild the `keygen.bin` in its original C language.

#### main():
- We do not use the argc, or argv arguments to main, so we cast those to void.
- We make room on the stack for our user input of size 0x68.
- Then use `puts` to print the prompt: `Give me a pass`.
- `scanf` saves user input into our `str_input` variable.
- An `if` statement checks the result of the call to `check_keys` function which determines if password, passed as an argument to the function, is acceptable.
- If true, the program prints the success prompt: `You made it, now keygen me!`.
- The program then proceeds, in either case, to return 0 and end.

#### check_keys():
- This function takes one argument, a char pointer to our user's input.
- Initializes an int variable called `sum` set to 0.
- The program then checks `if` the string length of the argument, `str_input` is less than or equal to 7. Or if the string length is greater than 10 characters long. If either of these conditions are true, the program prints the failure message: `Nope <3`, and exits with the 0 code.
- Otherwise the program proceeds to a for loop, with an integer `i` counter that starts at 0. This variable we can see is saved in `rbp-0x18`, and iterates over our `str_input`.
- For each char, the program adds the ascii value of the character to the `sum` variable. Adding up the total ascii value of all characters in the string.
- Then checks if the sum is less than or equal to 999. If so, the program once more prints the failure message: `Nope <3`, and exits with the 0 code.
- Otherwise the function returns true, where the `main` function handles printing the success message.

## Analysis
- The use of `scanf()` without checking the size of user input or resizing the array accordingly leads to the possibility of a stack overflow.
- We should use an input function that takes a length bound, such as `fgets`, or specify the size of the buffer in the format string for `scanf`.
- Stylistically, each of the print statements should have a line break at the end of them.