#!/usr/bin/env python3.7
import sys
import subprocess
import string
import random

def pw_generator():
    ascii_val = 0
    password = ''
    
    while ascii_val <= 999:
        if len(password) >= 10:
            i = 0
            password = ''
            ascii_val = 0
       
        for i in range(10):
            c = ''.join(random.sample(string.ascii_letters + '0123456789' + '!$%&()*+,-.:;<=>?@[]^_`{|}~', 1))
            ascii_val += ord(c)
            password += c

    return password

def main():
    password = pw_generator()
    print('Generated password: ' + password)
    subprocess.run('./keygen.bin', input=password, text=True)
    print('')
    return

if __name__ == "__main__":
    main()
