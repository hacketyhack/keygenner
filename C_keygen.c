#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool check_keys(char *str_input /*rbp-0x28*/){
    int sum = 0;                /*rbp-0x14*/
    
    if(strlen(str_input) <= 7 || strlen(str_input) > 10){
        printf("Nope <3");
        exit(0);
    }

    for (unsigned int i = 0 /*rbp-0x18*/; i < strlen(str_input); i++){
            sum += str_input[i];
    }

    if(sum <= 999){
        printf("Nope <3");
        exit(0);
    }

    return true;
}

int main(int argc, char** argv){
    (void)argc;
    (void)argv;

    //Both of these strings should have a \n at the end for cleaner output
    char str_input[0x68];
    
    // ask for user input here and save
    puts("Give me a pass");
    // This could stack overflow, or if no input use of unitialized memory
    scanf("%s", str_input);

    if (check_keys(str_input)){
        printf("You made it, now keygen me!");
    }
    
    return 0;
}
